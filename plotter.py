import re
import pandas as pd

file_name = "a.log"

file = open(file_name,"r")
data=[]
current={}

def appendTime(match):
    current['time'] = match[1]

# add the values to the current dictionary using regex groups
def append_RAM(match):
    current['ram_used'] = int(match[1])
    current['ram_free'] = int(match[2])
    current['ram_freeable'] = int(match[3])

for line in file.readlines():
    # the output of "get system status" shows system time
    match = re.match(r"System time:\s\D\D\D\s(.*)\n", line)
    if match is not None:
        # if 'time' is already in current dictionary then we have to add to the list and start working on a new timeframe
        # In other words, 'time' is a delimiter for the timeframes
        if 'time' in current:
            data.append(current)
            current = {} 
        appendTime(match)

    # the output of "get sys perf status" contains the line showing memory usage and it starts with "Memory:""
    match = re.match(r"Memory:.*\s([0-9]*)k used.*\s([0-9]*)k free.*\s([0-9]*)k freeable", line)
    if match is not None:
        append_RAM(match)
#print(data)

# convert the result list of dictionaries to a DataFrame object
df = pd.DataFrame(data)
# convert "time" column to datetime format
df['time'] = pd.to_datetime(df['time'], format='%b %d %X %Y')
print(df.info())
print(df.tail())


import matplotlib.pyplot as plt
import matplotlib.dates as mdates
from matplotlib.ticker import FormatStrFormatter

fig, ax = plt.subplots(1,2,figsize=(16,160))
fig.set_facecolor('lightgrey')

# draw lines for RAM usage
ax[0].plot(df['time'], df['ram_used'], 'r-', label="RAM used")
ax[0].plot(df['time'], df['ram_free'], 'b-', label="RAM Free")

# set format for X and Y axes' labels
ax[0].yaxis.set_major_formatter(FormatStrFormatter('%d'))
ax[0].xaxis.set_major_formatter(mdates.DateFormatter('%d%b-%Hh'))

# rotate labels for X axis ticks so they don't overlap
plt.setp(ax[0].xaxis.get_majorticklabels(), rotation=45)
ax[0].legend(loc='best')

plt.show()