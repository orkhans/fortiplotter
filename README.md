# Introduction

Fortiplotter is meant to parse the output of various diag commands from Fortigate firewalls and build the graphs
showing the resource usage change over time.

The project is still under development. Once ready for use the README file will be updated with the corresponding info.