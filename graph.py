import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
from matplotlib.ticker import FormatStrFormatter

class Graph():
    def __init__(self, data):
        # convert the list of dictionaries to a DataFrame object
        self.df = pd.DataFrame(data)
        # convert "time" column to datetime format
        self.df['time'] = pd.to_datetime(self.df['time'], format='%b %d %X %Y')

    def draw(self):
        fig, ax = plt.subplots(1,2,figsize=(16,160))
        fig.set_facecolor('lightgrey')

        # draw lines for RAM usage
        ax[0].plot(self.df['time'], self.df['ram_used'], 'r-', label="RAM used")
        ax[0].plot(self.df['time'], self.df['ram_free'], 'b-', label="RAM Free")

        # set format for X and Y axes' labels
        ax[0].yaxis.set_major_formatter(FormatStrFormatter('%d'))
        ax[0].xaxis.set_major_formatter(mdates.DateFormatter('%d%b-%Hh'))

        # rotate labels for X axis ticks so they don't overlap
        plt.setp(ax[0].xaxis.get_majorticklabels(), rotation=45)
        ax[0].legend(loc='best')

        plt.show()